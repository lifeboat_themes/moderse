#Moderse

##Home Page

###Slider Collections
You may have up to 3 collections that are used in a slider on the home page. Choose up to three collections to be shown, in the custom Fields labelled
**Home: Slider Collection 1,
Home: Slider Collection 2,
Home: Slider Collection 3.**

_You may also change the image shown on the slider by setting the image in the field labelled, in the collection of choice; **Slider Image**_

###Carousel Collections
Under the Display section, you may have up to 3 collections to be featured in tabbed carousels. 1 Collection is shown at any given time with the option to click another tab to see a different collection, each in its own carousel.

To add these collections, select up to 3 collections in the fields labelled **Home: Carousel Collection 1, Home: Carousel Collection 2, Home: Carousel Collection 3**

 
###Banner Collections
Under the slider section of the home page, 2 collections may be shown next to each other. These collections can be set by selecting a collection in the fields labelled:
**Home: Banner Collection 1, Home: Banner Collection 2.**

_You may also change the image shown on the banner by setting the image in the field labelled, in the collection of choice; **Banner Image**_

##Footer
### Footer: First Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the left of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Second Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown in the middle of the three menu options.

_Note: Submenus items will not be displayed_

###Footer: Widget
A widget can be placed to be shown on the right-hand side of the footer, for example a map.
This is done by editing the HTML custom field labelled **Footer: Widget**

##Contact Page
###Map Location
In the Contact page, you can have a map of the store displayed underneath the Contact form.
This can be done by going to the custom field labelled **Map Location** in the Design section and inputting the store name and full address in the field, in the format shown below.

**Store Name, Street Name, Location Post Code**

_NOTE: It is important to get these details exactly as they are shown on Google Maps._

##Collection
###Collection Heading
A collection may have a heading that is visible in the Slider Collections. This is done by typing in a Heading in the field labelled **Collection Heading** when editing the collection of choice.