<% if $Theme.CustomField('CollectionCard1').Value() && $Theme.CustomField('CollectionCard2').Value() && $Theme.CustomField('CollectionCard3').Value() %>
    <section class="banner_section mt-5">
        <div class="container">
            <div class="row">
                <% with $Theme.CustomField('CollectionCard1').Value() %>
                    <div class="col-lg-4 col-md-6">
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <% if $CustomField('CardImage').Value() %>
                                    <a href="$AbsoluteLink"><img src="$CustomField('CardImage').Value().Fill(600,389).Link" alt="$Title"/></a>
                                <% else %>
                                    <a href="$AbsoluteLink"><img src="$Image.Fill(600,389).Link" alt="$Title"/></a>
                                <% end_if %>
                                <div class="banner_content">
                                    <p>$CustomField('CollectionHeading').Value()</p>
                                    <h2>$Title</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                <% end_with %>
                <% with $Theme.CustomField('CollectionCard2').Value() %>
                    <div class="col-lg-4 col-md-6">
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <% if $CustomField('CardImage').Value() %>
                                    <a href="$AbsoluteLink"><img src="$CustomField('CardImage').Value().Fill(600,389).Link" alt="$Title"/></a>
                                <% else %>
                                    <a href="$AbsoluteLink"><img src="$Image.Fill(600,389).Link" alt="$Title"/></a>
                                <% end_if %>
                                <div class="banner_content">
                                    <p>$CustomField('CollectionHeading').Value()</p>
                                    <h2>$Title</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                <% end_with %>
                <% with $Theme.CustomField('CollectionCard3').Value() %>
                    <div class="col-lg-4 col-md-6">
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <% if $CustomField('CardImage').Value() %>
                                    <a href="$AbsoluteLink"><img src="$CustomField('CardImage').Value().Fill(600,389).Link" alt="$Title"/></a>
                                <% else %>
                                    <a href="$AbsoluteLink"><img src="$Image.Fill(600,389).Link" alt="$Title"/></a>
                                <% end_if %>
                                <div class="banner_content">
                                    <p>$CustomField('CollectionHeading').Value()</p>
                                    <h2>$Title</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                <% end_with %>
            </div>
        </div>
    </section>
<% end_if %>