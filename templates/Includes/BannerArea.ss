<% if $Collection.CustomField('BannerImage').Value() %>
    <section class="banner_fullwidth black_fullwidth" style="background: url('$Collection.CustomField('BannerImage').AbsoluteURL') no-repeat scroll center center/cover;">
<% else %>
    <section class="banner_fullwidth black_fullwidth" style="background: url('$Collection.Image.AbsoluteURL') no-repeat scroll center center/cover;">
<% end_if %>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="banner_text">
                    <h2>$Collection.Title</h2>
                    <span>$Collection.CustomField('BannerHeading').Value()</span>
                    <a href="$Collection.AbsoluteLink">SHop now</a>
                </div>
            </div>
        </div>
    </div>
</section>